# PlainAcct Orchestration

[Docker Compose](https://docs.docker.com/compose/) orchestration files for [PlainAcct](https://gitlab.com/petrzelenka/plainacct).


## Development Environment

The app consists of two containers:

- PlainAcct web app served by the internal Django development server
- [PostgreSQL](https://www.postgresql.org) database server

The web app container is build from the `./app` subdirectory. This directory is not under this repository, allowing either creating the subdirectory and cloning the PlainAcct repository into it or creating a symlink pointing to the directory with PlainAcct source code.

To build the containers, use

	docker-compose -p plainacct-dev -f docker-compose-dev.yml build

To start the containers, use

	docker-compose -p plainacct-dev -f docker-compose-dev.yml up -d


## Production Environment

The app consists of three containers:

- PlainAcct web app served by [Gunicorn](https://gunicorn.org)
- [PostgreSQL](https://www.postgresql.org) database server
- [NGINX](https://www.nginx.com) acting as reverse proxy and serving PlainAcct static files

The web app container is build directly from the PlainAccct GitLab repository.

To build the containers, use

	docker-compose -p plainacct -f docker-compose-prod.yml build

To start the containers, use

	docker-compose -p plainacct -f docker-compose-prod.yml up -d


## Configuring the App

In a production environment, first create the `DJANGO_SECRET_KEY` environment variable with [a Django secret key for your installation](https://docs.djangoproject.com/en/3.2/ref/settings/#secret-key).

Then, run the following command in the web app container:

To apply database migrations, run

	python manage.py migrate

To collect static files in a production environment, run

	python manage.py collectstatic --clear

To create app superuser, run

	python manage.py createsuperuser
